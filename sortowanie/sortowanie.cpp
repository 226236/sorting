#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>

using namespace std;

//funkcja wypelniajaca tablice losowymi liczbami
void uzupelnij(long long int tab[],int rozmiar)
{
     srand(time(NULL));
     for(int k=0;k<rozmiar;k++)
    {
        tab[k]=((rand()% 50) +1);
    }
}

//algorytm sortowania babelkowego
void babelkowe(long long int tab[],int poczatek,int koniec)
{
    int schowek, zamiana;
    do
    {
        zamiana=0;
        for(int i=poczatek;i<koniec;i++)
        {
            if(tab[i]>tab[i+1])
            {
                zamiana=zamiana+1;
                schowek=tab[i];
                tab[i]=tab[i+1];
                tab[i+1]=schowek;
            }
        }
    }while(zamiana!=0);
}

//algorytm sortowania przez wstawianie
void wstawianie(long long int tablica[],  int poczatek, int koniec)
{
int i,j,schowek;
for (i=poczatek;i<koniec;i++)
    {
        j=i;
        schowek=tablica[i];
        while ((j>0) && (tablica[j-1]>schowek))
            {
                tablica[j]=tablica[j-1];
                j--;
            }
        tablica[j]=schowek;
    }
}

//funkcja rozdzielajaca tablice i przygotowujaca do sortowania mergesort
void merge(int pocz, int sr, int kon, long long int tab[], long long t[])
{
int i,j,q;
for (i=pocz; i<=kon; i++) t[i]=tab[i];
i=pocz; j=sr+1; q=pocz;
while (i<=sr && j<=kon) {
if (t[i]<t[j])
tab[q++]=t[i++];
else
tab[q++]=t[j++];
}
while (i<=sr) tab[q++]=t[i++];
}

//algorytm mergesortu tab[pocz...kon]
void mergesort(int pocz, int kon, long long int tab[], long long int t[])
{
int sr;
if (pocz<kon) {
sr=(pocz+kon)/2;
mergesort(pocz, sr,tab,t);
mergesort(sr+1, kon,tab,t);
merge(pocz, sr, kon, tab, t);
}
}

void Pokaz(long long int tab[],int rozmiar)
{
    for(int i=0;i<rozmiar;i++)
    {
        cout <<tab[i]<<endl;
    }
}

void Przepisz(long long int tab[], long long int tmp[],int rozmiar)
{
    for(int i=0;i<rozmiar;i++)
    {
        tab[i]=tmp[i];
    }
}

int main()
{
    LARGE_INTEGER frequency;
    LARGE_INTEGER t1,t2;
    double elapsedTime;
    QueryPerformanceFrequency(&frequency);
    double odwracacz;
    int liczba=5;
    int tablicaRoz[liczba]={10000,25000,50000,75000,100000};
    int rozmiar=tablicaRoz[4];
    //int rozmiar=10;
    long long int tab[rozmiar];
    long long int tmp[rozmiar];
    uzupelnij(tab,rozmiar);
    //cout <<"Przed sortowaniem: "<<endl;
    //Pokaz(tab,rozmiar);
    //cout<<endl;

    //mergesort(0,rozmiar-1,tab,tmp);
    babelkowe(tab,0,rozmiar);
    for(int m=0;m<rozmiar/2;m++)
    {
        odwracacz=tab[rozmiar-m-1];
        tab[rozmiar-m-1]=tab[m];
        tab[m]=odwracacz;
    }
    //wstawianie(tab,0,rozmiar);
    Przepisz(tmp,tab,rozmiar);

        //cout <<"Tablica posortowana w 50%: "<<endl;
        //Pokaz(tab,rozmiar);
        QueryPerformanceCounter(&t1);
        for(int i=0;i<1;i++)
        {
            babelkowe(tab,0,rozmiar);
            //wstawianie(tab,0,rozmiar);
            //mergesort(0,rozmiar-1,tab,tmp);
            //cout <<"Tablica posortowana: "<<endl;
            //Pokaz(tab,rozmiar);
            Przepisz(tab,tmp,rozmiar);
        }
        QueryPerformanceCounter(&t2);
        elapsedTime=(t2.QuadPart-t1.QuadPart)*1000.0/frequency.QuadPart;
        cout<<elapsedTime<<"ms.\n"<<endl;
    return 0;
}
